import { useState } from 'react'
import './Notes/notes.jsx';
import './CreateNotes/createnotes.jsx';
import './CreateUser/createuser.jsx';

function App() {
  const [App] = useState(0)

  return (
    <>
      <Notes/>
      <CreateNotes/>
      <CreateUser/>
    </>
  )
}

export default App
